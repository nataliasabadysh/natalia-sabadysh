import { NextResponse } from "next/server";

const middleware = () => {
  const response = NextResponse.next();
  response.cookies.set("user", "true");

  return response;
};

export { middleware };
