import { cookies } from "next/headers";
import About from "./about/page";
import Profile from "./profile/page";
import styles from "./page.module.css";

const Home = () => {
  const userExists = cookies().get("user");
  const title = userExists
    ? "Привіт, раді бачити тебе знову!"
    : "Ласкаво просимо!";

  return (
    <div>
      <h1>{title}</h1>
      <About />
      <Profile />
    </div>
  );
};

export default Home;
